#!/bin/bash

cd /var/www/teslafinal
export NODE_ENV=test
export PORT=3000

node server.js
