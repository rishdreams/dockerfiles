#!/bin/bash


cd /var/www/dream11api && export NODE_ENV=test

pm2 start pm2.json

pm2 logs
